" This wipes all existing settings. This means that if a setting in this file
" is removed, then it will return to default. In other words, this file serves
" as an enforced single point of truth for Tridactyl's configuration.
" sanitize tridactyllocal tridactylsync

" Just use a blank page for new tab. It would be nicer to use the standard
" Firefox homepage, but Tridactyl doesn't support this yet.
set newtab about:newtab

" Use vim in tmux for editor.
set editorcmd terminal -e tmux -u new vim -S /home/bertino/.vim/tridactyl.vim

" Ctrl-F should use the browser's native 'find' functionality.
unbind <C-f>

" But also support Tridactyl search too.
bind / fillcmdline find
bind ? fillcmdline find -?
bind n findnext 1
bind N findnext -1
" Remove search highlighting.
bind ,<Space> nohlsearch
" Use smart case.
set findcase smart

" Smooth scrolling, yes please. This is still a bit janky in Tridactyl.
set smoothscroll true

" The default jump of 10 is a bit much.
bind j scrollline 7
bind k scrollline -7

" Set l and ; for left and right (HOME ROW BABY)
bind l scrollpx -100 0
bind ; scrollpx 100 0

" Set C-l and C-; to back and forward
bind <C-l> back
bind <C-;> forward

" K and J should move between tabs. x should close them.
bind J tabprev
bind K tabnext
bind x tabclose

" Don't run Tridactyl on some web sites because it doesn't work well, or
" because the web site has its own keybindings.
autocmd DocStart mail.google.com mode ignore

" Sometimes the status bar in the bottom left corner overlaps the Tridactyl
" command line, so set an option to move the status bar to the right.
guiset_quiet hoverlink right

" Add helper commands that Mozillians think make Firefox irredeemably
" insecure. For details, read the comment at the top of this file.
command fixamo_quiet jsb tri.excmds.setpref("privacy.resistFingerprinting.block_mozAddonManager", "true").then(tri.excmds.setpref("extensions.webextensions.restrictedDomains", '""'))
command fixamo js tri.excmds.setpref("privacy.resistFingerprinting.block_mozAddonManager", "true").then(tri.excmds.setpref("extensions.webextensions.restrictedDomains", '""').then(tri.excmds.fillcmdline_tmp(3000, "Permissions added to user.js. Please restart Firefox to make them take affect.")))

" Make Tridactyl work on more sites at the expense of some security. For
" details, read the comment at the top of this file.
fixamo_quiet
