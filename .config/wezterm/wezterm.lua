local wezterm = require 'wezterm'
local config = {}

if wezterm.config_builder then
    config = wezterm.config_builder()
end

config.font = wezterm.font_with_fallback({ 'FiraCode Retina', 'FiraCodeNerdFont' })
config.font_size = 10.0
config.font_rules = {
    {
        intensity = 'Normal',
        italic = true,
        font = wezterm.font({ family = 'Fira Code', style = 'Italic' })
    },
    {
        intensity = 'Bold',
        italic = false,
        font = wezterm.font({ family = 'FiraCode Nerd Font', weight = 'Bold' })
    },
    {
        intensity = 'Bold',
        italic = true,
        font = wezterm.font({ family = 'FiraCode', weight = 'Bold', style = 'Italic' })
    }
}

config.enable_csi_u_key_encoding = true
config.enable_tab_bar = false
config.window_decorations = 'RESIZE'
config.hide_mouse_cursor_when_typing = false
config.window_close_confirmation = 'NeverPrompt'
config.color_scheme = 'OneDarkest'

config.keys = {
    {
        key = 'Backspace',
        mods = 'CTRL',
        action = wezterm.action.SendKey({ key = 'W', mods = 'CTRL' })
    },
    {
        key = 'Backspace',
        mods = 'SHIFT',
        action = wezterm.action.SendKey({ key = 'Backspace' })
    }
}

config.window_padding = {
    top = 0,
    bottom = 0,
    left = 0,
    right = 0,
}

return config
