return {
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v3.x',
        lazy = true,
        keys = {
            { "<leader>f", ":LspZeroFormat<CR>" }
        },
    },
    {
        'L3MON4D3/LuaSnip',
    },
    {
        'hrsh7th/nvim-cmp',
        dependencies = {
            'hrsh7th/cmp-emoji',
            'hrsh7th/cmp-path',
            'hrsh7th/cmp-buffer',
            'L3MON4D3/LuaSnip',
            { 'js-everts/cmp-tailwind-colors', opts = {} },
        },
        config = function()
            require('lsp-zero').extend_cmp()

            local cmp_action = require('lsp-zero.cmp').action()
            local cmp = require('cmp')
            local cmp_autopairs = require('nvim-autopairs.completion.cmp')
            cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())
            cmp.setup({
                sources = {
                    { name = 'nvim_lsp' },
                    { name = 'luasnip' },
                    { name = 'buffer' },
                    { name = 'path' },
                },
                mapping = {
                    ['<CR>'] = cmp.mapping.confirm(),
                    ['<Tab>'] = cmp_action.luasnip_next_or_expand(),
                    ['<S-Tab>'] = cmp_action.luasnip_shift_supertab(),
                },
                formatting = {
                    format = require("cmp-tailwind-colors").format,
                }
            })
        end,
    },
    {
        'neovim/nvim-lspconfig',
        cmd = 'LspInfo',
        event = { 'BufReadPre', 'BufNewFile' },
        dependencies = {
            { 'VonHeikemen/lsp-zero.nvim' },
            { 'hrsh7th/cmp-nvim-lsp' },
            { 'williamboman/mason-lspconfig.nvim' },
            {
                'williamboman/mason.nvim',
                build = ':MasonUpdate',
            },
        },
        config = function()
            local lsp_zero = require('lsp-zero')

            lsp_zero.set_sign_icons({
                error = '',
                warn = '',
                hint = '',
                info = '',
            })

            lsp_zero.extend_lspconfig()

            lsp_zero.on_attach(function(_, bufnr)
                lsp_zero.default_keymaps({ buffer = bufnr, preserve_mappings = false })
            end)

            require('mason').setup({})

            local lspconfig = require('lspconfig')

            require('mason-lspconfig').setup({
                handlers = {
                    lsp_zero.default_setup,

                    lua_ls = function()
                        lspconfig.lua_ls.setup(lsp_zero.nvim_lua_ls())
                    end,

                    tailwindcss = function()
                        lspconfig.tailwindcss.setup({
                            filetypes = { "rust", "html" },
                            init_options = {
                                userLanguages = { rust = "html" },
                            }
                        })
                    end,

                    rust_analyzer = function()
                        lspconfig.rust_analyzer.setup({
                            settings = {
                                ["rust-analyzer"] = {
                                    cargo = {
                                        features = "all",
                                        allFeatures = true,
                                    }
                                }
                            }
                        })
                    end,

                    pylsp = function()
                        lspconfig.pylsp.setup({
                            settings = {
                                pylsp = {
                                    plugins = {
                                        pycodestyle = { enabled = false },
                                    }
                                }
                            }
                        })
                    end,

                    emmet_language_server = function()
                        lspconfig.emmet_language_server.setup({
                            filetypes = { "css", "html", "javascript", "rust" },
                        })
                    end,
                }
            })
        end
    },
    {
        "nvimtools/none-ls.nvim",
        event = { 'BufReadPre', 'BufNewFile' },
        dependencies = { "mason.nvim", "nvim-lua/plenary.nvim" },
        opts = function()
            local nls = require("null-ls")
            return {
                root_dir = require("null-ls.utils").root_pattern(".null-ls-root", ".neoconf.json", "Makefile", ".git"),
                sources = {
                    nls.builtins.formatting.stylua,
                    nls.builtins.formatting.shfmt,
                    nls.builtins.formatting.leptosfmt,
                    nls.builtins.formatting.prettier,
                },
            }
        end,
    },
    { "folke/neodev.nvim", opts = {} }
}
