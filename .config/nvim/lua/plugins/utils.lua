return {
    {
        'kana/vim-arpeggio',
        lazy = false
    },
    {
        'declancm/windex.nvim',
        keys = {
            { "<C-j>",  "<cmd>lua require('windex').switch_window('down')<cr>" },
            { "<C-k>",  "<cmd>lua require('windex').switch_window('up')<cr>" },
            { "<C-l>",  "<cmd>lua require('windex').switch_window('left')<cr>" },
            { "<C-h>",  "<cmd>lua require('windex').switch_window('right')<cr>" },
            { "<C-w>=", "<cmd>lua require('windex').toggle_nvim_maximize()<cr>" },
        },
        opts = {
            default_keymaps = false,
        }
    },
    {
        "abecodes/tabout.nvim",
        event = "InsertEnter",
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
            "hrsh7th/nvim-cmp",
        },
        opts = {}
    },
    {
        'nvim-telescope/telescope.nvim',
        branch = '0.1.x',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'nvim-telescope/telescope-fzf-native.nvim'
        },
        keys = {
            { '<C-p>', '<cmd>Telescope find_files<cr>' },
            { '<C-f>', '<cmd>Telescope current_buffer_fuzzy_find<cr>' },
            { '<C-g>', '<cmd>Telescope live_grep<cr>' },
        }
    },
    {
        'machakann/vim-sandwich'
    },
    {
        "chrisgrieser/nvim-spider",
        lazy = true,
        keys = {
            { 'w', "<cmd>lua require('spider').motion('w')<cr>", mode = { 'n', 'o', 'x' } },
            { 'e', "<cmd>lua require('spider').motion('e')<cr>", mode = { 'n', 'o', 'x' } },
            { 'b', "<cmd>lua require('spider').motion('b')<cr>", mode = { 'n', 'o', 'x' } },
            { 'ge', "<cmd>lua require('spider').motion('ge')<cr>", mode = { 'n', 'o', 'x' } },
        }
    },
    {
        'Pocco81/auto-save.nvim',
        opts = {
            execution_message = {
                message = ""
            },
            condition = function(buf)
                local fn = vim.fn
                local utils = require("auto-save.utils.data")

                if fn.getbufvar(buf, "&modifiable") == 1
                    and
                    utils.not_in(fn.getbufvar(buf, "&filetype"), { 'git' })
                then
                    return true
                end
                return false
            end,
        }
    },
    {
        'windwp/nvim-autopairs',
        event = "InsertEnter",
        opts = {}
    },
    {
        'echasnovski/mini.comment',
        opts = {
            options = {
                ignore_blank_line = false,
            },
            mappings = {
                comment = 'gc',
                comment_line = '<leader>c',
                comment_visual = '<leader>c',
            }
        },
        keys = {
            { '<leader>c', mode = {"n", "v"} },
        }
    },
    {
        'echasnovski/mini.bufremove',
    },
    {
        'mbbill/undotree',
        keys = {
            { '<leader>u', '<cmd>UndotreeToggle<cr>' }
        }
    }
}
