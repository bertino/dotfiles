return {
    {
        'nvim-lualine/lualine.nvim',
        lazy = false,
        dependencies = { 'navarasu/onedark.nvim' },
        config = function()
            local lualine_onedark = require('lualine.themes.onedark')

            lualine_onedark.normal.c.bg = '#000000'
            lualine_onedark.normal.b.bg = '#121212'

            local function maximize_status()
                return vim.t.maximized and ' ' or ''
            end

            vim.opt.showmode = false

            require('lualine').setup({
                options = {
                    theme = lualine_onedark
                },

                sections = {
                    lualine_b = {'diff', 'diagnostics'},
                    lualine_c = { { 'filename', path = 1 }, maximize_status },
                    lualine_x = { 'encoding', 'filetype', { require("noice").api.statusline.mode.get, cond = require("noice").api.statusline.mode.has }}
                }
            })
        end
    },
    {
        'folke/noice.nvim',
        event = 'VeryLazy',
        opts = {
            lsp = {
                override = {
                    ['vim.lsp.util.convert_input_to_markdown_lines'] = true,
                    ['vim.lsp.util.stylize_markdown'] = true,
                    ['cmp.entry.get_documentation'] = true,
                },
            },
            presets = {
                bottom_search = true
            },
            cmdline = {
                view = 'cmdline'
            }
        },
        dependencies = {
            'MunifTanjim/nui.nvim',
            'rcarriga/nvim-notify',
        }
    },
    {
        'rcarriga/nvim-notify',
        lazy = false,
        keys = {
            {
                '<leader>r',
                function()
                    require('notify').dismiss({ silent = true, pending = true })
                end,
                desc = 'Dismiss all Notifications',
            },
        },
        opts = {
            timeout = 3000,
            max_height = function()
                return math.floor(vim.o.lines * 0.75)
            end,
            max_width = function()
                return math.floor(vim.o.columns * 0.75)
            end,
            background_color = '#000000',
            top_down = false
        },
        config = function(_, opts)
            vim.o.termguicolors = true
            require('notify').setup(opts)
        end
    },
    {
        'akinsho/bufferline.nvim',
        event = 'VeryLazy',
        keys = {
            { '<leader>bp', '<Cmd>BufferLineTogglePin<CR>',            desc = 'Toggle pin' },
            { '<leader>bP', '<Cmd>BufferLineGroupClose ungrouped<CR>', desc = 'Delete non-pinned buffers' },
        },
        opts = {
            options = {
                close_command = function(n) require('mini.bufremove').delete(n, false) end,
                right_mouse_command = function(n) require('mini.bufremove').delete(n, false) end,
                diagnostics = 'nvim_lsp',
                always_show_bufferline = false,
                offsets = {
                    {
                        filetype = 'neo-tree',
                        text = 'Neo-tree',
                        highlight = 'Directory',
                        text_align = 'left',
                    },
                },
            },
        },
    },
    {
        'nvim-tree/nvim-tree.lua',
        lazy = false,
        dependencies = {
            { 'nvim-tree/nvim-web-devicons', lazy = true },
        },
        opts = {},
        keys = {
            {
                '<leader>n',
                '<cmd>NvimTreeFindFileToggle<cr>',
                desc = 'Open NvimTree at the current files location'
            }
        },
    },
    {
        'folke/which-key.nvim',
        opts = {
            defaults = {
                ['<leader>sn'] = { name = '+noice' },
            }
        }
    },
    {
        'echasnovski/mini.indentscope',
        event = { 'BufReadPre', 'BufNewFile' },
        opts = {
            -- symbol = '│',
            -- symbol = '⎸',

            symbol = '▏',
            options = { try_as_border = true },
            draw = { delay = 50 }
        },
        init = function()
            vim.api.nvim_create_autocmd('FileType', {
                pattern = {
                    'help',
                    'alpha',
                    'dashboard',
                    'neo-tree',
                    'Trouble',
                    'lazy',
                    'mason',
                    'notify',
                    'toggleterm',
                    'lazyterm',
                },
                callback = function()
                    vim.b.miniindentscope_disable = true
                end,
            })
        end,
    },
    {
        'lukas-reineke/indent-blankline.nvim',
        main = "ibl",
        event = { 'BufReadPost', 'BufNewFile' },
        opts = {
            indent = {
                char = '▏',
                -- char = '│',
            },
            scope = { enabled = false },
            exclude = {
                filetypes = {
                    'help',
                    'alpha',
                    'dashboard',
                    'Trouble',
                    'lazy',
                    'mason',
                    'notify',
                    'toggleterm',
                    'lazyterm',
                }
            },
        },
    },
    {
        "jasha10/vim-symlink",
        branch = 'neovim0.9-autocmd-issue',
        dependencies = { "moll/vim-bbye" }
    },
    {
        'tpope/vim-fugitive',
        cmd = 'Git',
        keys = { '<leader>g', '<cmd>Git<cr>' },
        config = function()
            vim.keymap.set('', '<leader>g', '<cmd>Git<cr>')
        end
    },
    {
        'lewis6991/gitsigns.nvim',
        opts = {
            on_attach = function(bufnr)
                local gs = package.loaded.gitsigns

                local function map(mode, l, r, opts)
                    opts = opts or {}
                    opts.buffer = bufnr
                    vim.keymap.set(mode, l, r, opts)
                end

                map('n', ']c', function()
                    if vim.wo.diff then return ']c' end
                    vim.schedule(function() gs.next_hunk() end)
                    return '<Ignore>'
                end, { expr = true })

                map('n', '[c', function()
                    if vim.wo.diff then return '[c' end
                    vim.schedule(function() gs.prev_hunk() end)
                    return '<Ignore>'
                end, { expr = true })

                map({ 'n', 'v' }, '<leader>hs', gs.stage_hunk)
                map({ 'n', 'v' }, '<leader>hr', gs.reset_hunk)
                map('n', '<leader>hS', gs.stage_buffer)
                map('n', '<leader>hu', gs.reset_hunk)
                map('n', '<leader>hR', gs.reset_buffer)
                map('n', '<leader>hp', gs.preview_hunk)
                map('n', '<leader>hb', function() gs.blame_line { full = true } end)
                map('n', '<leader>b', gs.toggle_current_line_blame)
                map('n', '<leader>hd', gs.diffthis)
                map('n', '<leader>hD', function() gs.diffthis('~') end)
                map('n', '<leader>d', gs.toggle_deleted)

                map({ 'o', 'x' }, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
            end
        }
    },
    {
        "folke/trouble.nvim",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        opts = {},
        cmd = "Trouble",
        keys = {
            { "<leader>t", "<cmd>Trouble diagnostics toggle<cr>" },
        }
    },
    {
        'stevearc/dressing.nvim',
        opts = {},
    },
    {
        'MeanderingProgrammer/render-markdown.nvim',
        dependencies = { 'nvim-treesitter/nvim-treesitter', 'nvim-tree/nvim-web-devicons' },
        ft = { 'markdown', 'pandoc' },
        opts = {
            render_modes = true,
        },
    },
    {
        'tadmccorkle/markdown.nvim',
        ft = { 'markdown', 'pandoc' },
        opts = {}
    },
}
