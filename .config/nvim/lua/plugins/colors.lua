return {
    {
        'navarasu/onedark.nvim',
        config = function()
            require('onedark').setup({
                style = 'darker',
                transparent = true,

                colors = {
                    bg0 = '#000000',
                    bg1 = '#121212',
                    bg2 = '#232323',
                    bg3 = '#333333',
                    fg = '#bcbcbc',
                    red = '#ff5f87',
                    dark_red = '#be4463',
                    orange = '#d7875f',
                    yellow = '#d7af87',
                    dark_yellow = '#9f8163',
                    green = '#7ad475',
                    dark_green = '#639f63',
                    cyan = '#00afd7',
                    blue = '#00afff',
                    purple = '#d75fd7',
                    dark_purple = '#9f449f',
                    magenta = '#c397d8',
                    white = '#afafaf',
                    black = '#000000',
                    grey = '#434548',
                    light_grey = '#636568',
                },

                highlights = {
                    GitSignsChange = { fg = '$yellow' },
                    GitSignsChangeLn = { fg = '$yellow' },
                    GitSignsChangeNr = { fg = '$yellow' },
                    CocInlayHint = { fg = '$dark_purple' },
                    DiffAdd = { fg = '$black', bg = '$dark_green' },
                    DiffDelete = { fg = '$black', bg = '$dark_red' },
                    DiffChange = { fg = '$yellow', bg = '$none', fmt = 'underline' },
                    DiffText = { fg = '$black', bg = '$yellow' },
                    Folded = { fg = '$grey' },
                    NotifyBackground = { bg = '$bg1' },
                    IblScope = { fg = '$bg2' },
                    IblIndent = { fg = '$bg2' },
                    IblWhitespace = { fg = '$bg2' },
                    RenderMarkdownH1Bg = { fg = '$red', bg = '$bg1' },
                    RenderMarkdownH2Bg = { fg = '$purple', bg = '$bg1' },
                    RenderMarkdownH3Bg = { fg = '$orange', bg = '$bg1' },
                    RenderMarkdownH4Bg = { fg = '$red', bg = '$bg1' },
                    RenderMarkdownH5Bg = { fg = '$purple', bg = '$bg1' },
                    RenderMarkdownH6Bg = { fg = '$orange', bg = '$bg1' },
                },

                diagnostics = {
                    background = false,
                }
            })
            require('onedark').load()
            vim.o.termguicolors = true
        end
    },
    {
        'brenoprata10/nvim-highlight-colors',
        opts = {
            enable_tailwind = true,
        }
    }
}
