#! /bin/bash

sudo pacman -Syu --noconfirm base-devel git neovim
cd ~
mkdir git
cd git
git clone https://aur.archlinux.org/paru-bin
cd paru-bin
makepkg -si
paru -S --noconfirm fd ripgrep thefuck keychain man tmux trash-cli fzf python-pynvim nodejs oh-my-zsh-git neovim-plug nvimpager-git
sudo chsh -s $(which zsh) bertino
sudo git clone https://github.com/Aloxaf/fzf-tab /usr/share/oh-my-zsh/custom/plugins/fzf-tab
cd ../
git clone https://github.com/gpakosz/.tmux
ln -rsf .tmux/.tmux.conf ~/.config/tmux/tmux.conf
