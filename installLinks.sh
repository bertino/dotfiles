#!/bin/bash

# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
find ./ -type d -exec mkdir -p ~/{} \;
find . -type f,l -regextype posix-extended ! -regex '.*(\.sh|\.git\/|\.gitmodules).*' -exec  ln -rs {} ~/{} \;
